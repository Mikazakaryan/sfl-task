// Redirect to users page
const onUsersClick = () => {
  window.location.href = `${getBaseUrl()}?url=users`;
};

// Redirect to repos page
const onReposClick = () => {
  window.location.href = `${getBaseUrl()}?url=repos`;
};

// Redirect to users page, if no url query
if (!getQueryValue("url")) {
  onUsersClick();
}

// Setting navigation active element
if (getQueryValue("url") === "users" || getQueryValue("url") === "user") {
  addClass(document.getElementById("navigation-users"), "navigation-active");
  removeClass(document.getElementById("navigation-repos"), "navigation-active");
} else if (getQueryValue("url") === "repos") {
  addClass(document.getElementById("navigation-repos"), "navigation-active");
  removeClass(document.getElementById("navigation-users"), "navigation-active");
}
