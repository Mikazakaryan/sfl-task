/*
  Redux like store management
  Separated for each page
*/

const initUsersState = () => {
  let users = [];
  let page = 0;
  let maxPage = 0;

  return {
    setUsers: (value) => {
      users = value;
    },
    getUsers: () => users.slice(page * 10, page * 10 + 10),
    getPage: () => page,
    setPage: (value) => {
      page = value;
    },
    incrementPage: () => {
      if (page < maxPage) ++page;
    },
    decrementPage: () => {
      if (page !== 0) --page;
    },
    setMaxPage: (value) => {
      maxPage = Math.floor(value);
    },
  };
};

const initUserState = () => {
  let userInfo = {};
  let repos = [];
  let page = 0;
  let maxPage = 0;

  return {
    getPage: () => page,
    setPage: (value) => {
      page = value;
    },
    setUserInfo: (value) => {
      userInfo = value;
    },
    setRepos: (value) => {
      repos = value;
    },
    getUserInfo: () => userInfo,
    getRepos: () => repos.slice(page * 20, page * 20 + 20),
    incrementPage: () => {
      if (page < maxPage) ++page;
    },
    decrementPage: () => {
      if (page !== 0) --page;
    },
    setMaxPage: (value) => {
      maxPage = Math.floor(value);
    },
  };
};

const initReposState = () => {
  let repos = [];
  let page = 0;
  let maxPage = 0;

  return {
    getPage: () => page,
    setPage: (value) => {
      page = value;
    },
    setRepos: (value) => {
      repos = value;
    },
    getRepos: () => repos.slice(page * 20, page * 20 + 20),
    incrementPage: () => {
      if (page < maxPage) ++page;
    },
    decrementPage: () => {
      if (page !== 0) --page;
    },
    setMaxPage: (value) => {
      maxPage = Math.floor(value);
    },
  };
};
