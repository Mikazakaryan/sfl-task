/*
  This file is for reusable functions
*/

// Get base url from location
const getBaseUrl = () => window.location.href.split("?")[0];

// Get query value from url, return empty string in case of query not exist
const getQueryValue = (query = "search") => {
  try {
    return window.location.href.split(`${query}=`)[1].split("&")[0];
  } catch (error) {
    return "";
  }
};

// Simple back action
const onBack = () => {
  window.history.back();
};

// Add class on element if it not exists
const addClass = (element, className) => {
  if (!element) return;
  if (!element.classList.contains(className)) {
    element.classList.add(className);
  }
};

// Remove class from element if it exists
const removeClass = (element, className) => {
  if (!element) return;
  if (element.classList.contains(className)) {
    element.classList.remove(className);
  }
};

// Creating reusable search element, with text param, on enter action param and default value param
const createSearchElement = (text, onEnter, initialValue) => {
  const searchInput = document.createElement("input");

  searchInput.placeholder = `Search for ${text}`;
  searchInput.className = "search-input";
  searchInput.addEventListener("keypress", (event) => {
    if (event.keyCode === 13) {
      event.preventDefault();
      onEnter(event.target.value);
    }
  });

  if (initialValue) searchInput.value = initialValue;

  return searchInput;
};

// Creating arrow with text and onClick action, text right near icon
// isRotated param is for left direction arrow
const createArrowWithText = (onClick, text, isRotated) => {
  const arrow = document.createElement("div");
  arrow.className = `pagination-arrow ${isRotated ? "rotate" : ""}`;

  const div = document.createElement("div");
  div.innerHTML = text;

  const wrapper = document.createElement("div");
  wrapper.className = "pagination-pair-wrapper";
  wrapper.onclick = onClick;

  if (isRotated) wrapper.appendChild(arrow);
  wrapper.appendChild(div);
  if (!isRotated) wrapper.appendChild(arrow);

  return wrapper;
};

// Creating pagination element with both next and prev actions
const createPaginationElement = (onPrev, onNext) => {
  const wrapper = document.createElement("div");
  wrapper.className = "pagination-wrapper";

  const prevEl = createArrowWithText(onPrev, "Prev", true);
  const nextEl = createArrowWithText(onNext, "Next");

  wrapper.appendChild(prevEl);
  wrapper.appendChild(nextEl);

  return wrapper;
};

// Creating reusable user box, with avatar, username and on click function if needed
const createUserBoxElement = (avatar, username, onClick, isBigSize) => {
  const wrapper = document.createElement("div");
  wrapper.className = `users-box ${isBigSize ? "big-user-box" : ""}`;
  if (onClick) wrapper.onclick = onClick;

  const avatarElement = document.createElement("img");
  avatarElement.src = avatar;
  avatarElement.className = "users-avatar";

  const usernameText = document.createElement("div");
  usernameText.className = `users-name ${isBigSize ? "big-user-name" : ""}`;
  usernameText.innerHTML = username;

  wrapper.appendChild(avatarElement);
  wrapper.appendChild(usernameText);

  return wrapper;
};

// Creating reusable pagination line element, which is "createPaginationElement" element with wrapped styles
const paginationLine = (onPrev, onNext) => {
  const line = document.createElement("div");
  line.className = "pagination-line";

  const linePagination = createPaginationElement(onPrev, onNext);
  line.appendChild(linePagination);

  return line;
};

// Creating reusable pagination line element, which is "createPaginationElement" and "createSearchElement" element with wrapped styles
const paginationLineWithSearch = (
  searchText,
  onSearch,
  searchValue,
  onPrev,
  onNext
) => {
  const line = document.createElement("div");
  line.className = "pagination-line-with-search";

  const search = createSearchElement(searchText, onSearch, searchValue);

  const linePagination = createPaginationElement(onPrev, onNext);

  line.appendChild(search);
  line.appendChild(linePagination);

  return line;
};

// Creating reusable repo list element, with optional pagination
const renderReposList = (list, onPrev, onNext, withPagination = false) => {
  const reposWrapper = document.createElement("div");
  reposWrapper.id = "repos-list-wrapper";

  list.forEach((element) => {
    const rowWrapper = document.createElement("div");
    rowWrapper.className = "repo-row-wrapper";

    const name = document.createElement("div");
    name.className = "repo-row-item";
    name.innerHTML = element.name;

    const url = document.createElement("a");
    url.className = "repo-row-item";
    url.target = "_blank";
    url.href = element.html_url;
    url.innerHTML = element.html_url;

    rowWrapper.appendChild(name);
    rowWrapper.appendChild(url);

    reposWrapper.appendChild(rowWrapper);
  });

  const contentWrapper = document.createElement("div");

  if (withPagination) {
    const topPagination = paginationLine(onPrev, onNext);
    contentWrapper.appendChild(topPagination);
  }

  contentWrapper.appendChild(reposWrapper);

  if (withPagination) {
    const botPagination = paginationLine(onPrev, onNext);
    contentWrapper.appendChild(botPagination);
  }

  return contentWrapper;
};

// Redirect to single user page
const onUserClick = (username) => {
  window.location.href = `${getBaseUrl()}?url=user&username=${username}`;
};
