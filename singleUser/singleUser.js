// Pagination prev
const onSingleUserPrev = (userState) => {
  userState.decrementPage();
  const usernameValue = getQueryValue("username");
  window.location.href = `${getBaseUrl()}?url=user&username=${usernameValue}&page=${userState.getPage()}`;
};

// Pagination next
const onSingleUserNext = (userState) => {
  userState.incrementPage();
  const usernameValue = getQueryValue("username");
  window.location.href = `${getBaseUrl()}?url=user&username=${usernameValue}&page=${userState.getPage()}`;
};

// Rendering users info and repos
const renderUser = (userState) => {
  const mainElement = document.getElementById("main");

  const backIcon = createArrowWithText(onBack, "Back To Users", true);

  const user = userState.getUserInfo();

  const userBox = createUserBoxElement(user.avatar_url, user.login, null, true);

  const infoBox = document.createElement("div");
  infoBox.className = "single-user-info-box";

  const bioElement = document.createElement("div");
  bioElement.innerHTML = user.bio;

  infoBox.appendChild(bioElement);

  const repoCountElement = document.createElement("div");
  repoCountElement.innerHTML = `Public Repos Count: ${user.public_repos}`;

  infoBox.appendChild(repoCountElement);

  const gistCountElement = document.createElement("div");
  gistCountElement.innerHTML = `Public Gists Count: ${user.public_gists}`;

  infoBox.appendChild(gistCountElement);

  const infoWrapper = document.createElement("div");
  infoWrapper.className = "single-user-info-wrapper";

  infoWrapper.appendChild(userBox);
  infoWrapper.appendChild(infoBox);

  const repostListElement = renderReposList(
    userState.getRepos(),
    () => onSingleUserPrev(userState),
    () => onSingleUserNext(userState),
    true
  );

  mainElement.appendChild(backIcon);
  mainElement.appendChild(infoWrapper);
  mainElement.appendChild(repostListElement);
};

// Starting rendering, also fetching user info and repos
const initiateUserPage = async (username, pageValue, userState) => {
  if (!pageValue)
    window.location.href = `${getBaseUrl()}?url=user&username=${username}&page=${userState.getPage()}`;

  const userInfo = await fetch(`https://api.github.com/users/${username}`);
  const parsedUserInfo = await userInfo.json();
  userState.setUserInfo(parsedUserInfo);

  const userRepos = await fetch(
    `https://api.github.com/users/${username}/repos`
  );
  const parsedUserRepo = await userRepos.json();
  userState.setRepos(parsedUserRepo);
  userState.setMaxPage(
    parsedUserRepo.length % 20
      ? parsedUserRepo.length / 20
      : parsedUserRepo.length / 20 - 1
  );

  renderUser(userState);
};

// If url is equal to user, it starts render of the page, gets all default values from query
if (getQueryValue("url") === "user") {
  const userState = initUserState();
  const usernameValue = getQueryValue("username");
  const pageValue = getQueryValue("page");

  if (pageValue) userState.setPage(pageValue);

  if (usernameValue) initiateUserPage(usernameValue, pageValue, userState);
  else onUsersClick();
}
