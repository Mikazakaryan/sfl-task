// Rendering repos list
const renderRepos = (reposState) => {
  const pageContent = document.getElementById("page-content");

  if (document.getElementById("repos-list"))
    pageContent.removeChild(document.getElementById("repos-list"));

  const reposList = document.createElement("div");
  reposList.id = "repos-list";

  reposList.appendChild(renderReposList(reposState.getRepos()));
  pageContent.appendChild(reposList);
};

// Changing query in case of search
const onRepoSearchChange = (reposState, value) => {
  window.location.href = `${getBaseUrl()}?url=repos&search=${value}&page=${reposState.getPage()}`;
};

// Fetching the search result
const onRepoSearch = async (reposState, value) => {
  const fetchedData = await fetch(
    `https://api.github.com/search/repositories?q=${value}`
  );

  const parsedData = await fetchedData.json();

  reposState.setMaxPage(
    parsedData.items.length % 20
      ? parsedData.items.length / 20
      : parsedData.items.length / 20 - 1
  );
  reposState.setRepos(parsedData.items);

  renderRepos(reposState);
};

// Pagination prev
const onReposPrev = (reposState) => {
  reposState.decrementPage();
  const searchValue = getQueryValue("search");
  onRepoSearchChange(reposState, searchValue);
};

// Pagination next
const onReposNext = (reposState) => {
  reposState.incrementPage();
  const searchValue = getQueryValue("search");
  onRepoSearchChange(reposState, searchValue);
};

// Starting rendering
const initiateReposPage = (reposState, searchValue) => {
  const mainElement = document.getElementById("main");

  const content = document.createElement("div");
  content.id = "page-content";

  mainElement.appendChild(
    paginationLineWithSearch(
      "repos",
      (value) => onRepoSearchChange(reposState, value),
      searchValue,
      () => onReposPrev(reposState),
      () => onReposNext(reposState)
    )
  );

  mainElement.appendChild(content);
  mainElement.appendChild(
    paginationLine(
      () => onReposPrev(reposState),
      () => onReposNext(reposState)
    )
  );

  renderRepos(reposState);
};

// If url is equal to repos, it starts render of the page, gets all default values from query
if (getQueryValue("url") === "repos") {
  const reposState = initReposState();
  const pageValue = getQueryValue("page");
  const searchValue = getQueryValue("search");

  if (pageValue) reposState.setPage(pageValue);

  initiateReposPage(reposState, searchValue);

  if (searchValue) onRepoSearch(reposState, searchValue);
}
