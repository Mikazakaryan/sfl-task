// Rendering users grid
const renderUsers = (getUsers) => {
  const users = getUsers();
  const pageContent = document.getElementById("page-content");

  if (document.getElementById("users-grid"))
    pageContent.removeChild(document.getElementById("users-grid"));

  const usersGrid = document.createElement("div");
  usersGrid.id = "users-grid";

  pageContent.appendChild(usersGrid);

  users.forEach((user) => {
    const userBox = createUserBoxElement(user.avatar_url, user.login, () =>
      onUserClick(user.login)
    );
    usersGrid.appendChild(userBox);
  });
};

// Changing query in case of search
const onSearchChange = (userState, value) => {
  window.location.href = `${getBaseUrl()}?url=users&search=${value}&page=${userState.getPage()}`;
};

// Fetching the search result
const onSearch = async (userState, value) => {
  const fetchedData = await fetch(
    `https://api.github.com/search/users?q=${value}`
  );

  const parsedData = await fetchedData.json();

  userState.setMaxPage(
    parsedData.items.length % 10
      ? parsedData.items.length / 10
      : parsedData.items.length / 10 - 1
  );
  userState.setUsers(parsedData.items);

  renderUsers(userState.getUsers);
};

// Pagination prev
const onPrev = (userState) => {
  userState.decrementPage();
  const searchValue = getQueryValue("search");
  onSearchChange(userState, searchValue);
};

// Pagination next
const onNext = (userState) => {
  userState.incrementPage();
  const searchValue = getQueryValue("search");
  onSearchChange(userState, searchValue);
};

// Starting rendering
const initiateUsersPage = (userState, searchValue) => {
  const mainElement = document.getElementById("main");

  const content = document.createElement("div");
  content.id = "page-content";

  mainElement.appendChild(
    paginationLineWithSearch(
      "users",
      (value) => onSearchChange(userState, value),
      searchValue,
      () => onPrev(userState),
      () => onNext(userState)
    )
  );

  mainElement.appendChild(content);
  mainElement.appendChild(
    paginationLine(
      () => onPrev(userState),
      () => onNext(userState)
    )
  );

  renderUsers(userState.getUsers);
};

// If url is equal to users, it starts render of the page, gets all default values from query
if (getQueryValue("url") === "users") {
  const userState = initUsersState();

  const searchValue = getQueryValue("search");
  const pageValue = getQueryValue("page");

  if (pageValue) userState.setPage(pageValue);

  initiateUsersPage(userState, searchValue);
  if (searchValue) {
    onSearch(userState, searchValue);
  }
}
